use std::collections::HashMap;
use std::time::Instant;
use serde::{Serialize, Deserialize};

// new protocol
#[derive(Serialize, Deserialize, Debug)]
pub enum ClientUpdateMessage {
    BubbleControlForceUpdate {
        force: Vector,
    },
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum WorldUpdateMessage {
    ClientInit {
        id: BubbleId,
    },
    BubbleCreate {
        id: BubbleId,
        position: Point,
    },
    BubblePositionUpdate {
        id: BubbleId,
        position: Point,
    },
    FoodCreate {
        id: FoodId,
        position: Point,
        mass: f64,
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BubbleId(String);

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct FoodId(String);

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Point {
    x: f64,
    y: f64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Vector {
    x: f64,
    y: f64,
}

impl BubbleId {

    pub fn new(id: String) -> Self {
        Self(id)
    }

    pub fn get_string(&self) -> &String {
        &self.0
    }
}

impl FoodId {

    pub fn new(id: String) -> Self {
        Self(id)
    }
}

impl From<&mikey_geometry::Point> for Point {
    fn from(point: &mikey_geometry::Point) -> Self {
        Self {
            x: point.x(),
            y: point.y(),
        }
    }
}

impl From<mikey_geometry::Point> for Point {
    fn from(point: mikey_geometry::Point) -> Self {
        Self {
            x: point.x(),
            y: point.y(),
        }
    }
}

impl From<&Point> for mikey_geometry::Point {
    fn from(point: &Point) -> Self {
        Self::new(point.x, point.y)
    }
}

impl From<mikey_geometry::Vector> for Vector {
    fn from(vector: mikey_geometry::Vector) -> Self {
        Self {
            x: vector.x(),
            y: vector.y(),
        }
    }
}

impl From<Vector> for mikey_geometry::Vector {
    fn from(vector: Vector) -> Self {
        Self::new(vector.x, vector.y)
    }
}