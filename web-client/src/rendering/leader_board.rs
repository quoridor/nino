use mikey_geometry::Point;
use crate::color::Color;
use crate::components::dimensions::DimensionsComponent;
use crate::components::leader_board::LeaderBoardComponent;
use crate::components::position::PositionComponent;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_leader_board(world: &World, rendering_context: &mut RenderingContext) {
    world
        .objects_by_component::<LeaderBoardComponent>()
        .for_each(|board| {
            let board_position = board.component::<PositionComponent>().unwrap();
            let rating = board.component::<LeaderBoardComponent>().unwrap().rating();

            let mut y = board_position.point().y();
            for (id, rating_item) in rating.iter().enumerate() {
                rendering_context.draw_text(
                    &Point::new(board_position.point().x(), y),
                    &Color::blue(),
                    &((id + 1).to_string() + ". " + rating_item),
                );
                y += 50.0;
            }
        });
}
