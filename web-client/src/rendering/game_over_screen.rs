use crate::components::bubble::BubbleComponent;
use crate::components::controllable_bubble::ControllableBubbleComponent;
use crate::components::eliminated::EliminatedBubbleComponent;
use crate::components::leader_board::LeaderBoardComponent;
use crate::components::position::PositionComponent;
use crate::rendering_context::RenderingContext;
use crate::world::{World, WorldObject};
use serde_json::de::Read;

pub fn render_game_over_screen(world: &World, rendering_context: &mut RenderingContext) {
    let mut competitors = world
        .objects_by_component::<ControllableBubbleComponent>()
        .peekable();

    if competitors.peek().is_some() {
        let player = competitors.next().unwrap();

        if player.contains_component::<EliminatedBubbleComponent>() {
            rendering_context.clear();
            rendering_context.draw_prompt_text(&format!(
                "game over, total mass: {}",
                player.component::<BubbleComponent>().unwrap().mass()
            ));
        }
    }
}
