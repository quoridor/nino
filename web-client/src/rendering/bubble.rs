use mikey_geometry::Angle;
use crate::components::bubble::BubbleComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::position::PositionComponent;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_bubbles(world: &World, rendering_context: &mut RenderingContext) {
    let angle = Angle::zero();
    for bubble in world.objects_by_component::<BubbleComponent>() {
        let position = bubble.component::<PositionComponent>().unwrap();
        let dimensions = bubble
            .component::<DimensionsComponent>()
            .unwrap()
            .dimensions();
        let bubble = bubble.component::<BubbleComponent>().unwrap();

        let dimensions = dimensions * (bubble.mass() / 100.0) as f64;

        rendering_context.draw_image_with_rotation(
            bubble.image(),
            position.point(),
            &dimensions,
            &angle,
        );
    }
}
