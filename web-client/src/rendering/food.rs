use crate::components::food::FoodComponent;
use crate::components::position::PositionComponent;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_food(world: &World, rendering_context: &mut RenderingContext) {
    world
        .objects_by_component::<FoodComponent>()
        .for_each(|food| {
            let food_position = food.component::<PositionComponent>().unwrap();
            let food_dot = food.component::<FoodComponent>().unwrap();

            rendering_context.draw_circle(
                food_position.point(),
                food_dot.mass() as f64,
                food_dot.color(),
            );
        });
}
