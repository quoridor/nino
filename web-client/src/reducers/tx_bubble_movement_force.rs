use nino_common::protocol;
use nino_common::protocol::ClientUpdateMessage;
use crate::{InputContext, NetworkContext, World};
use crate::components::bubble_movement_force::BubbleMovementForceComponent;
use crate::components::tx_movement_force::TxMovementForce;
use crate::utils::console_log;

pub fn tx_bubble_movement_force(world: &mut World, delta: f64, _input_context: &InputContext, network_context: &NetworkContext) {
    for obj in world.objects_by_component_mut::<TxMovementForce>() {
        let force = obj.component::<BubbleMovementForceComponent>().unwrap()
            .force()
            .clone();

        let tx_movement_force = obj.component_mut::<TxMovementForce>().unwrap();
        if tx_movement_force.tick(delta) && tx_movement_force.force(force.clone()) {
            network_context.send_message(&ClientUpdateMessage::BubbleControlForceUpdate {
                force: protocol::Vector::from(force),
            });
        }
    }
}