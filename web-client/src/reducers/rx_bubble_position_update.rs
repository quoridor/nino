use mikey_geometry::Point;
use nino_common::protocol::WorldUpdateMessage;
use crate::{console_log, Id, IdComponent, InputContext, NetworkContext, PositionComponent, World, WorldObject};
use crate::components::position_sync_force::PositionSyncForceComponent;

pub fn rx_bubble_position_update_reducer(world: &mut World, _delta: f64, _input_context: &InputContext, network_context: &NetworkContext) {
    network_context.incoming_messages_mut().retain(|msg| {
        if let WorldUpdateMessage::BubblePositionUpdate { id, position } = msg {
            let bubble_object: &mut WorldObject = match world.objects_by_component_mut::<IdComponent>()
                .filter(|obj| obj.component::<IdComponent>().unwrap().id() == &Id::new(id.get_string().clone()))
                .next() {
                Some(v) => v,
                None => return true,
            };
            let position_on_client: Point = bubble_object.component::<PositionComponent>().unwrap().point().clone();

            bubble_object.component_mut::<PositionSyncForceComponent>().unwrap()
                .set((Point::from(position) - position_on_client).to_vector());

            false
        } else {
            true
        }
    });
}