use mikey_geometry::Point;
use crate::components::bubble::BubbleComponent;
use crate::components::bubble_movement_force::BubbleMovementForceComponent;
use crate::components::bubble_velocity::BubbleVelocityComponent;
use crate::components::controllable_bubble::ControllableBubbleComponent;
use crate::input::InputContext;
use crate::network::NetworkContext;
use crate::world::World;

pub fn bubble_movement_force_reducer(
    world: &mut World,
    delta: f64,
    input_context: &InputContext,
    _network_context: &NetworkContext,
) {
    world
        .objects_by_component_mut::<ControllableBubbleComponent>()
        .for_each(|bubble| {
            let bubble = bubble.component_mut::<BubbleMovementForceComponent>().unwrap();

            let (_, x, y) = input_context.mouse();
            let cursor_position = Point::new(
                (x - input_context.client_x() / 2) as f64,
                (y - input_context.client_y() / 2) as f64,
            );

            let movement_force = cursor_position.to_vector();
            let magnitude = movement_force.magnitude();

            let mut movement_force = movement_force.normalize();
            if magnitude < 250.0 {
                movement_force *= magnitude / 250.0;
            }

            bubble.set_force(movement_force);
        });
}
