use nino_common::protocol::WorldUpdateMessage;
use crate::{AttachPositionComponent, CameraComponent, ControllableBubbleComponent, Id, IdComponent, InputContext, NetworkContext, World, WorldObject};
use crate::components::position_sync_force::PositionSyncForceComponent;
use crate::components::tx_movement_force::TxMovementForce;
use crate::utils::console_log;

pub fn rx_client_init(world: &mut World, _delta: f64, _input_context: &InputContext, network_context: &NetworkContext) {
    network_context.incoming_messages_mut().retain(|msg| {
        if let WorldUpdateMessage::ClientInit { id } = msg {
            let id = Id::new(id.get_string().clone());

            world.objects_by_component_mut::<CameraComponent>()
                .for_each(|obj| obj.add_component(
                    AttachPositionComponent::new(id.clone())
                ));

            let bubble_object: Option<&mut WorldObject> = world.objects_by_component_mut::<IdComponent>()
                .filter(|obj| obj.component::<IdComponent>().unwrap().id() == &id)
                .next();

            let controllable_bubble = ControllableBubbleComponent::new();
            let tx_movement_force = TxMovementForce::new();

            if let Some(bubble_object) = bubble_object {
                bubble_object.add_component(controllable_bubble);
                bubble_object.add_component(tx_movement_force);
            } else {
                world.add_object(WorldObject::new()
                    .with_component(IdComponent::new(id.clone()))
                    .with_component(controllable_bubble)
                    .with_component(tx_movement_force))
            }

            false
        } else {
            true
        }
    });
}