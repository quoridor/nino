use crate::{InputContext, NetworkContext, World};
use crate::components::position_sync_force::PositionSyncForceComponent;

pub fn tick_position_sync_reducer(world: &mut World, delta: f64, _input_context: &InputContext, network_context: &NetworkContext) {
    world.objects_by_component_mut::<PositionSyncForceComponent>()
        .map(|v| v.component_mut::<PositionSyncForceComponent>().unwrap())
        .for_each(|v| v.tick(delta));
}