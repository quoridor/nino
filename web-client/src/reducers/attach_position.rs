use mikey_geometry::Vector;
use crate::components::attach_position::AttachPositionComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::id::{Id, IdComponent};
use crate::components::position::PositionComponent;
use crate::input::InputContext;
use crate::network::NetworkContext;
use crate::world::World;

pub fn attach_position_reducer(
    world: &mut World,
    _: f64,
    _: &InputContext,
    _network_context: &NetworkContext,
) {
    let attached_objects: Vec<(Id, Id)> = world
        .objects_by_component::<AttachPositionComponent>()
        .map(|v| {
            (
                v.component::<IdComponent>().expect("expected id component to be present").id().clone(),
                v.component::<AttachPositionComponent>()
                    .expect("expected attach position component to be present")
                    .attach_to()
                    .clone(),
            )
        })
        .collect();

    for (attached_object_id, attached_to_id) in &attached_objects {
        let position = match world
            .objects_by_component::<IdComponent>()
            .filter(|obj| obj.component::<IdComponent>().unwrap().id() == attached_to_id)
            .filter(|obj| obj.contains_component::<PositionComponent>())
            .map(|obj| {
                obj.component::<PositionComponent>()
                    .unwrap()
                    .point()
                    .clone()
                    + match obj.component::<DimensionsComponent>() {
                        Some(v) => v.dimensions().to_vector() / 2.0,
                        None => Vector::zero(),
                    }
            })
            .next()
        {
            Some(v) => v,
            None => continue,
        };

        world
            .objects_by_component_mut::<IdComponent>()
            .filter(|obj| obj.component::<IdComponent>().unwrap().id() == attached_object_id)
            .for_each(|obj| {
                obj.component_mut::<PositionComponent>()
                    .unwrap()
                    .set(position.clone())
            })
    }
}
