use mikey_geometry::{Dimensions, Point};
use nino_common::protocol::WorldUpdateMessage;
use crate::{BubbleComponent, BubbleType, DimensionsComponent, Id, IdComponent, InputContext, NetworkContext, PositionComponent, World, WorldObject};
use crate::components::bubble_movement_force::BubbleMovementForceComponent;
use crate::components::position_sync_force::PositionSyncForceComponent;
use crate::utils::console_log;

pub fn rx_bubble_add_reducer(world: &mut World, _delta: f64, _input_context: &InputContext, network_context: &NetworkContext) {
    network_context.incoming_messages_mut().retain(|msg| {
        if let WorldUpdateMessage::BubbleCreate { id, position } = msg {
            let id = Id::new(id.get_string().clone());

            let bubble_object: Option<&mut WorldObject> = world.objects_by_component_mut::<IdComponent>()
                .filter(|obj| obj.component::<IdComponent>().unwrap().id() == &id)
                .next();

            let bubble_component = BubbleComponent::new("NakanoMiku", BubbleType::Miku, 100.0);
            let position_component = PositionComponent::new(Point::from(position));
            let dimensions_component = DimensionsComponent::new(Dimensions::new(150.0, 150.0));
            let movement_force_component = BubbleMovementForceComponent::new();
            let position_sync_force_component = PositionSyncForceComponent::new();

            if let Some(bubble_object) = bubble_object {
                bubble_object.add_component(bubble_component);
                bubble_object.add_component(position_component);
                bubble_object.add_component(dimensions_component);
                bubble_object.add_component(movement_force_component);
                bubble_object.add_component(position_sync_force_component);
            } else {
                world.add_object(WorldObject::new()
                    .with_component(IdComponent::new(id.clone()))
                    .with_component(bubble_component)
                    .with_component(position_component)
                    .with_component(dimensions_component)
                    .with_component(movement_force_component)
                    .with_component(position_sync_force_component));
            }

            false
        } else {
            true
        }
    });
}