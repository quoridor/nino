use crate::components::bubble::BubbleComponent;
use crate::components::bubble_movement_force::BubbleMovementForceComponent;
use crate::components::position::PositionComponent;
use crate::components::position_sync_force::PositionSyncForceComponent;
use crate::console_log;
use crate::input::InputContext;
use crate::network::NetworkContext;
use crate::world::World;

pub fn bubble_velocity_reducer(
    world: &mut World,
    delta: f64,
    _input_context: &InputContext,
    _network_context: &NetworkContext,
) {
    world
        .objects_by_component_mut::<BubbleComponent>()
        .for_each(|bubble| {
            let mass = bubble.component::<BubbleComponent>().unwrap().mass();

            let movement_force = bubble
                .component::<BubbleMovementForceComponent>()
                .unwrap()
                .force()
                .clone();
            let position_sync_force = bubble
                .component::<PositionSyncForceComponent>()
                .unwrap()
                .force();

            let combined_force = movement_force + position_sync_force;

            let position_component = bubble.component_mut::<PositionComponent>().unwrap();
            let position = position_component.point().clone();
            let position = position + (combined_force * delta * 64.0 / mass);

            position_component.set(position);
        })
}
