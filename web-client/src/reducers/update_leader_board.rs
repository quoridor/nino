use std::cmp::Ordering;
use crate::components::bubble::BubbleComponent;
use crate::components::controllable_bubble::ControllableBubbleComponent;
use crate::components::leader_board::LeaderBoardComponent;
use crate::input::InputContext;
use crate::network::NetworkContext;
use crate::world::World;

pub fn update_leader_board_reducer(
    world: &mut World,
    _delta: f64,
    _input_context: &InputContext,
    network_context: &NetworkContext,
) {
    let mut board = world
        .objects_by_component::<LeaderBoardComponent>()
        .next()
        .unwrap()
        .component::<LeaderBoardComponent>()
        .unwrap()
        .clone();
    let mut rating = world
        .objects_by_component::<BubbleComponent>()
        .map(|obj| obj.component::<BubbleComponent>().unwrap())
        .collect::<Vec<&BubbleComponent>>();

    rating.sort_by(|a, b| b.mass().partial_cmp(&a.mass()).unwrap_or(Ordering::Equal));

    let player_option = world
        .objects_by_component::<ControllableBubbleComponent>()
        .next();

    if player_option.is_some() {
        let player = player_option
            .unwrap()
            .component::<BubbleComponent>()
            .unwrap();

        let mut displayed = rating[0..(rating.len() % 11)]
            .iter()
            .map(|bubble| bubble.name().to_string())
            .collect::<Vec<String>>();

        if !board.rating().contains(&player.name().to_string()) {
            displayed.push(player.name().to_string());
        };
        board.update_rating(displayed);

        let mut board_v = world
            .objects_by_component_mut::<LeaderBoardComponent>()
            .next()
            .unwrap()
            .component_mut::<LeaderBoardComponent>()
            .unwrap();
        *board_v = board
    }
}
