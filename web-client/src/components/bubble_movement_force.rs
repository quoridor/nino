use mikey_geometry::Vector;

pub struct BubbleMovementForceComponent {
    force: Vector,
}

impl BubbleMovementForceComponent {
    pub fn new() -> Self {
        Self {
            force: Vector::zero(),
        }
    }

    pub fn force(&self) -> &Vector {
        &self.force
    }

    pub fn set_force(&mut self, force: Vector) {
        self.force = force;
    }
}