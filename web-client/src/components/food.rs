use crate::color::Color;

pub struct FoodComponent {
    color: Color,
    mass: u32,
}

impl FoodComponent {
    pub fn new(color: Color, mass: u32) -> Self {
        Self { color, mass }
    }

    pub fn color(&self) -> &Color {
        &self.color
    }

    pub fn mass(&self) -> u32 {
        self.mass
    }
}
