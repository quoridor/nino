use mikey_geometry::{Dimensions, Point, Vector};
use crate::rendering_context::Image;
use lazy_static::lazy_static;
use std::cmp::Ordering;

lazy_static! {
    static ref MIKU_BUBBLE: Image = Image::new(
        "/assets/nakano.miku.png".to_owned(),
        Point::new(0.0, 0.0),
        Dimensions::new(639.0, 639.0),
    );
    static ref NINO_BUBBLE: Image = Image::new(
        "/assets/nakano.nino.png".to_owned(),
        Point::new(0.0, 0.0),
        Dimensions::new(720.0, 720.0),
    );
    static ref ICHIKA_BUBBLE: Image = Image::new(
        "/assets/nakano.ichika.png".to_owned(),
        Point::new(0.0, 0.0),
        Dimensions::new(808.0, 808.0),
    );
    static ref YOUTSUBA_BUBBLE: Image = Image::new(
        "/assets/nakano.youtsuba.png".to_owned(),
        Point::new(0.0, 0.0),
        Dimensions::new(736.0, 736.0),
    );
    static ref ITSUKI_BUBBLE: Image = Image::new(
        "/assets/nakano.itsuki.png".to_owned(),
        Point::new(0.0, 0.0),
        Dimensions::new(674.0, 674.0),
    );
}

pub enum BubbleType {
    Miku,
    Nino,
    Youtsuba,
    Ichika,
    Itsuki,
}

pub struct BubbleComponent {
    name: String,
    mass: f64,
    image: Image,
}

impl From<BubbleType> for Image {
    fn from(bubble_type: BubbleType) -> Self {
        match bubble_type {
            BubbleType::Miku => MIKU_BUBBLE.clone(),
            BubbleType::Nino => NINO_BUBBLE.clone(),
            BubbleType::Ichika => ICHIKA_BUBBLE.clone(),
            BubbleType::Youtsuba => YOUTSUBA_BUBBLE.clone(),
            BubbleType::Itsuki => ITSUKI_BUBBLE.clone(),
        }
    }
}

impl BubbleComponent {
    pub fn new(name: &str, bubble_type: BubbleType, mass: f64) -> Self {
        Self {
            name: name.to_owned(),
            mass,
            image: Image::from(bubble_type),
        }
    }

    pub fn image(&self) -> &Image {
        &self.image
    }

    pub fn mass(&self) -> f64 {
        self.mass
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}
