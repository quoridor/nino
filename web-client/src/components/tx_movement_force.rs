use mikey_geometry::Vector;

pub struct TxMovementForce {
    delta: f64,
    force: Vector,
}

impl TxMovementForce {
    pub fn new() -> Self {
        Self {
            delta: 0.0,
            force: Vector::zero(),
        }
    }

    pub fn tick(&mut self, delta: f64) -> bool {
        self.delta += delta;
        if self.delta > 100.0 {
            self.delta = self.delta % 100.0;
            true
        } else {
            false
        }
    }

    pub fn force(&mut self, force: Vector) -> bool {
        if (&force - &self.force).magnitude() > 0.001 {
            self.force = force;
            true
        } else {
            false
        }
    }
}