use mikey_geometry::Vector;

pub struct PositionSyncForceComponent {
    position_delta: Vector,
    ticks_remaining: f64,
}

impl PositionSyncForceComponent {
    pub fn new() -> Self {
        Self {
            position_delta: Vector::zero(),
            ticks_remaining: 0.0,
        }
    }

    pub fn set(&mut self, delta: Vector) {
        self.position_delta = delta;
        self.ticks_remaining = 1000.0;
    }

    pub fn tick(&mut self, delta: f64) {
        if self.ticks_remaining >= 0.0 {
            self.ticks_remaining -= delta;
        }
    }

    pub fn force(&self) -> Vector {
        if self.ticks_remaining > 0.0 {
            &self.position_delta * 0.01
        } else {
            Vector::zero()
        }
    }
}