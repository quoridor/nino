#[derive(Clone)]
pub struct LeaderBoardComponent {
    rating: Vec<String>,
}

impl LeaderBoardComponent {
    pub fn new() -> Self {
        Self { rating: Vec::new() }
    }

    pub fn rating(&self) -> &Vec<String> {
        &self.rating
    }
    pub fn update_rating(&mut self, rating: Vec<String>) {
        self.rating = rating;
    }

    pub fn add_to_rating(&mut self, player_name: String) {
        self.rating.push(player_name);
    }
}
