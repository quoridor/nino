use mikey_geometry::{Point, Vector};

#[derive(Clone)]
pub struct PositionComponent {
    position: Point,
}

impl PositionComponent {
    pub fn new(position: Point) -> Self {
        Self { position }
    }

    pub fn point(&self) -> &Point {
        &self.position
    }

    pub fn set(&mut self, position: Point) {
        self.position = position;
    }

    pub fn add_vector(&mut self, vector: &Vector) {
        self.position += vector;
    }

    pub fn distance_to(&self, other: &PositionComponent) -> f64 {
        self.position.distance_to_point(other.point())
    }
}
