use crate::utils::console_log;
use nino_common::protocol::{ClientUpdateMessage, WorldUpdateMessage};
use std::cell::{Ref, RefCell, RefMut};
use std::rc::Rc;
use std::sync::Arc;
use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use web_sys::{MessageEvent, WebSocket};

pub struct NetworkContext {
    ws: WebSocket,
    incoming_messages: Rc<RefCell<Vec<WorldUpdateMessage>>>,
}

impl NetworkContext {
    pub fn new() -> Self {
        let incoming_messages = Rc::new(RefCell::new(Vec::new()));

        let ws = WebSocket::new("ws://localhost:8080/api/v1/world/ws").unwrap();
        let message_handler = {
            let _ws = ws.clone();
            let incoming_messages = incoming_messages.clone();

            Closure::wrap(Box::new(move |e: MessageEvent| {
                if let Ok(text) = e.data().dyn_into::<js_sys::JsString>() {
                    let text: String = text.into();
                    let msg: WorldUpdateMessage = serde_json::from_str(&text).unwrap();
                    incoming_messages.borrow_mut().push(msg);
                } else {
                    console_log("something arrived, don't know what exactly");
                }
            }) as Box<dyn FnMut(MessageEvent)>)
        };

        let open_handler = Closure::wrap(Box::new(move |_| {
            // Don't do anything
        }) as Box<dyn FnMut(JsValue)>);

        ws.set_onmessage(Some(message_handler.as_ref().unchecked_ref()));
        ws.set_onopen(Some(open_handler.as_ref().unchecked_ref()));

        message_handler.forget();
        open_handler.forget();

        Self {
            ws,
            incoming_messages,
        }
    }

    pub fn incoming_messages_mut(&self) -> RefMut<Vec<WorldUpdateMessage>> {
        self.incoming_messages.borrow_mut()
    }

    pub fn peek_incoming_message(&self) -> Option<WorldUpdateMessage> {
        self.incoming_messages.borrow().get(0).cloned()
    }

    pub fn remove_incoming_message(&self) {
        self.incoming_messages.borrow_mut().remove(0);
    }

    pub fn send_message(&self, message: &ClientUpdateMessage) {
        self.ws.send_with_str(&serde_json::to_string(&message).unwrap());
    }
}
