use crate::components::id::{Id, IdComponent};
use anymap::AnyMap;

pub struct World {
    objects: Vec<WorldObject>,
}

pub struct WorldObject {
    components: AnyMap,
}

impl World {
    pub fn new(objects: Vec<WorldObject>) -> Self {
        Self { objects }
    }

    pub fn empty() -> Self {
        Self::new(Vec::new())
    }

    pub fn add_object(&mut self, object: WorldObject) {
        self.objects.push(object);
    }

    pub fn remove_objects(&mut self, ids: &Vec<Id>) {
        self.objects.drain_filter(|wo| {
            let component_id = match wo.component::<IdComponent>() {
                Some(v) => v.id(),
                None => return false,
            };

            ids.contains(component_id)
        });
    }

    pub fn with_object(mut self, object: WorldObject) -> Self {
        self.add_object(object);
        self
    }

    pub fn objects(&self) -> impl Iterator<Item = &WorldObject> {
        self.objects.iter()
    }

    #[allow(dead_code)]
    pub fn objects_mut(&mut self) -> impl Iterator<Item = &mut WorldObject> {
        self.objects.iter_mut()
    }

    pub fn objects_by_component<T: 'static>(&self) -> impl Iterator<Item = &WorldObject> {
        self.objects().filter(|obj| obj.contains_component::<T>())
    }

    #[allow(dead_code)]
    pub fn objects_by_component_mut<T: 'static>(
        &mut self,
    ) -> impl Iterator<Item = &mut WorldObject> {
        self.objects_mut()
            .filter(|obj| obj.contains_component::<T>())
    }
}

impl WorldObject {
    pub fn new() -> Self {
        Self {
            components: AnyMap::new(),
        }
    }

    pub fn with_component<T: 'static>(mut self, component: T) -> Self {
        self.components.insert(component);
        self
    }

    pub fn add_component<T: 'static>(&mut self, component: T) {
        self.components.insert(component);
    }

    pub fn contains_component<T: 'static>(&self) -> bool {
        self.components.contains::<T>()
    }

    pub fn component<T: 'static>(&self) -> Option<&T> {
        self.components.get::<T>()
    }

    #[allow(dead_code)]
    pub fn component_mut<T: 'static>(&mut self) -> Option<&mut T> {
        self.components.get_mut::<T>()
    }
}
