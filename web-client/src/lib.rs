#![feature(box_syntax)]
#![feature(drain_filter)]

mod color;
#[allow(dead_code)]
mod components;
#[allow(dead_code)]
mod geometry;
#[allow(dead_code)]
mod input;
mod network;
mod reducers;
mod rendering;
mod rendering_context;
mod utils;
mod world;

use rand::Rng;
use std::cell::RefCell;
use std::f64;
use std::rc::Rc;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use mikey_geometry::{Dimensions, Point};
use crate::color::Color;
use crate::components::attach_position::AttachPositionComponent;
use crate::components::bubble::{BubbleComponent, BubbleType};
use crate::components::bubble_velocity::BubbleVelocityComponent;
use crate::components::camera::CameraComponent;
use crate::components::controllable_bubble::ControllableBubbleComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::eliminated::EliminatedBubbleComponent;
use crate::components::food::FoodComponent;
use crate::components::id::{Id, IdComponent};
use crate::components::leader_board::LeaderBoardComponent;
use crate::components::position::PositionComponent;
use crate::input::InputContext;
use crate::network::NetworkContext;
use crate::reducers::attach_position::attach_position_reducer;
use crate::reducers::bubble_movement_force::bubble_movement_force_reducer;
use crate::reducers::bubble_velocity::bubble_velocity_reducer;
use crate::reducers::rx_bubble_add::rx_bubble_add_reducer;
use crate::reducers::rx_bubble_position_update::rx_bubble_position_update_reducer;
use crate::reducers::rx_client_init::rx_client_init;
use crate::reducers::tick_position_sync::tick_position_sync_reducer;
use crate::reducers::tx_bubble_movement_force::tx_bubble_movement_force;
use crate::reducers::update_leader_board::update_leader_board_reducer;
use crate::rendering::bubble::render_bubbles;
use crate::rendering::food::render_food;
use crate::rendering::game_over_screen::render_game_over_screen;
use crate::rendering::leader_board::render_leader_board;
use crate::rendering_context::RenderingContext;
use crate::utils::{console_log, performance_now};
use crate::world::{World, WorldObject};

type WorldReducer = Box<dyn Fn(&mut World, f64, &InputContext, &NetworkContext)>;
type RenderingPipelineElement = Box<dyn Fn(&World, &mut RenderingContext)>;
type RequestAnimationClosure = Rc<RefCell<Option<Closure<dyn FnMut()>>>>;

#[wasm_bindgen(start)]
pub fn start() {
    run_main_loop(
        create_world(),
        create_world_reducers(),
        create_rendering_pipeline(),
        RenderingContext::new(),
        InputContext::new(),
        NetworkContext::new(),
    );
}

fn create_world() -> World {
    let mut world = World::empty()
        .with_object(
            WorldObject::new()
                .with_component(IdComponent::new(Id::new("camera")))
                .with_component(CameraComponent::new())
                .with_component(PositionComponent::new(Point::zero()))
        )
        .with_object(
            WorldObject::new()
                .with_component(PositionComponent::new(Point::new(0.0, 20.0)))
                .with_component(DimensionsComponent::new(Dimensions::new(100.0, 500.0)))
                .with_component(LeaderBoardComponent::new()),
        );
    let mut rand = rand::thread_rng();
    let mut gen_rand_coordinate = || (rand.gen::<i64>() % 1000) as f64 - 500.0;
    for _ in 0..100 {
        world = world.with_object(
            WorldObject::new()
                .with_component(PositionComponent::new(Point::new(
                    gen_rand_coordinate(),
                    gen_rand_coordinate(),
                )))
                .with_component(FoodComponent::new(Color::generate_rand_color(), 10)),
        );
    }
    world
}

fn create_world_reducers() -> Vec<WorldReducer> {
    vec![
        // rx
        box rx_client_init,
        box rx_bubble_add_reducer,
        box rx_bubble_position_update_reducer,

        // update
        box tick_position_sync_reducer,
        box bubble_movement_force_reducer,
        box bubble_velocity_reducer,
        box attach_position_reducer,

        // tx
        box tx_bubble_movement_force,
    ]
}

fn create_rendering_pipeline() -> Vec<RenderingPipelineElement> {
    vec![
        box render_food,
        box render_bubbles,
        box render_game_over_screen,
    ]
}

fn run_main_loop(
    mut world: World,
    world_reducers: Vec<WorldReducer>,
    rendering_pipeline: Vec<RenderingPipelineElement>,
    mut rendering_context: RenderingContext,
    input_context: InputContext,
    network_context: NetworkContext,
) {
    let f: RequestAnimationClosure = Rc::new(RefCell::new(None));
    let g = f.clone();

    *g.borrow_mut() = Some(Closure::wrap(Box::new(move || {
        let delta = rendering_context.compute_delta(performance_now());
        world_reducers
            .iter()
            .for_each(|reducer| reducer(&mut world, delta, &input_context, &network_context));

        rendering_context.on_new_frame(&world);
        rendering_pipeline
            .iter()
            .for_each(|element| element(&world, &mut rendering_context));

        // request next frame render
        request_animation_frame(f.clone());
    }) as Box<dyn FnMut()>));

    // request first render
    request_animation_frame(g.clone());
}

fn request_animation_frame(closure: RequestAnimationClosure) {
    web_sys::window()
        .unwrap()
        .request_animation_frame(closure.borrow().as_ref().unwrap().as_ref().unchecked_ref())
        .unwrap();
}
