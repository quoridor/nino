use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
use wasm_bindgen::closure::Closure;
use wasm_bindgen::JsCast;

const KEYCODE_W: u32 = 87;
const KEYCODE_A: u32 = 65;
const KEYCODE_S: u32 = 83;
const KEYCODE_D: u32 = 68;

const KEYCODE_UP: u32 = 38;
const KEYCODE_LEFT: u32 = 37;
const KEYCODE_DOWN: u32 = 40;
const KEYCODE_RIGHT: u32 = 39;

pub struct InputContext {
    state: Rc<RefCell<HashMap<u32, bool>>>,
    // (mouse key, x, y)
    mouse: Rc<RefCell<(i16, i32, i32)>>,
    client_x: i32,
    client_y: i32,
}

impl InputContext {
    pub fn new() -> Self {
        let state = Rc::new(RefCell::new(HashMap::new()));
        let mouse = Rc::new(RefCell::new((0, 0, 0)));

        let window = web_sys::window().unwrap();

        let keydown_handler = {
            let state = state.clone();

            Closure::wrap(Box::new(move |event: web_sys::KeyboardEvent| {
                state.borrow_mut().insert(event.key_code(), true);
            }) as Box<dyn FnMut(_)>)
        };

        let keyup_handler = {
            let state = state.clone();

            Closure::wrap(Box::new(move |event: web_sys::KeyboardEvent| {
                state.borrow_mut().insert(event.key_code(), false);
            }) as Box<dyn FnMut(_)>)
        };

        let body = window.document().unwrap().body().unwrap();
        let (client_x, client_y) = (body.client_width(), body.client_height());

        let mousemove_handler = {
            let mouse = mouse.clone();

            Closure::wrap(Box::new(move |event: web_sys::MouseEvent| {
                let mouse_data = (event.button(), event.client_x(), event.client_y());
                *mouse.borrow_mut() = mouse_data;
            }) as Box<dyn FnMut(_)>)
        };

        window
            .add_event_listener_with_callback("keydown", keydown_handler.as_ref().unchecked_ref())
            .unwrap();
        window
            .add_event_listener_with_callback("keyup", keyup_handler.as_ref().unchecked_ref())
            .unwrap();

        window
            .add_event_listener_with_callback(
                "mousemove",
                mousemove_handler.as_ref().unchecked_ref(),
            )
            .unwrap();

        keydown_handler.forget();
        keyup_handler.forget();
        mousemove_handler.forget();

        Self {
            state,
            mouse,
            client_x,
            client_y,
        }
    }

    pub fn is_key_down(&self, key_code: u32) -> bool {
        *self.state.borrow().get(&key_code).unwrap_or(&false)
    }

    pub fn is_key_down_w(&self) -> bool {
        self.is_key_down(KEYCODE_W)
    }

    pub fn is_key_down_a(&self) -> bool {
        self.is_key_down(KEYCODE_A)
    }

    pub fn is_key_down_s(&self) -> bool {
        self.is_key_down(KEYCODE_S)
    }

    pub fn is_key_down_d(&self) -> bool {
        self.is_key_down(KEYCODE_D)
    }

    pub fn is_key_down_up(&self) -> bool {
        self.is_key_down(KEYCODE_UP)
    }

    pub fn is_key_down_left(&self) -> bool {
        self.is_key_down(KEYCODE_LEFT)
    }

    pub fn is_key_down_down(&self) -> bool {
        self.is_key_down(KEYCODE_DOWN)
    }

    pub fn is_key_down_right(&self) -> bool {
        self.is_key_down(KEYCODE_RIGHT)
    }

    pub fn is_input_forward(&self) -> bool {
        self.is_key_down_w() || self.is_key_down_up()
    }

    pub fn is_input_turn_left(&self) -> bool {
        self.is_key_down_a() || self.is_key_down_left()
    }

    pub fn is_input_turn_right(&self) -> bool {
        self.is_key_down_d() || self.is_key_down_right()
    }

    pub fn is_input_backward(&self) -> bool {
        self.is_key_down_s() || self.is_key_down_down()
    }

    pub fn mouse(&self) -> (i16, i32, i32) {
        *self.mouse.as_ref().borrow()
    }

    pub fn client_x(&self) -> i32 {
        self.client_x
    }

    pub fn client_y(&self) -> i32 {
        self.client_y
    }
}
