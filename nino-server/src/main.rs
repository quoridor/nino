mod app;
mod redis;
mod world;
mod ws;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    app::run().await
}
