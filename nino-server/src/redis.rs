use std::env;
use log::error;
use redis::Commands;
use rand::Rng;
use serde::{Serialize, Deserialize};
use mikey_geometry::Point;

pub const WORLD_BLOCK_CHANNEL_PREFIX: &str = "nino::block_channel";

#[derive(Serialize, Deserialize)]
enum WorldEvent {
}

#[derive(Debug, Clone)]
struct BlockIndex {
    x: i32,
    y: i32,
}

pub struct RedisWorldStateStore {
    client: redis::Client,
    bubble_id: String,
}

impl RedisWorldStateStore {
    pub fn new() -> Self {
        Self {
            client: redis::Client::open(redis_connection_string()).unwrap(),
            bubble_id: uuid::Uuid::new_v4().to_string(),
        }
    }

    pub fn init(&mut self) {
        let position = Point::new(
            rand::thread_rng().gen_range(-1000.0..1000.0),
            rand::thread_rng().gen_range(-1000.0..1000.0),
        );
        // let block = BlockIndex::from_position(&self.position);

        // self.init_pubsub_thread(&block);
    }

    pub fn shutdown(&mut self) {
    }

    fn init_pubsub_thread(&mut self, block: &BlockIndex) {
        let mut connection = self.client.get_connection().unwrap();
        let mut pubsub = connection.as_pubsub();
        // think about it
    }
}

impl BlockIndex {
    pub fn new(x: i32, y: i32) -> Self {
        Self {
            x,
            y,
        }
    }

    pub fn from_position(position: &Point) -> Self {
        Self::new(
            (position.x() / 1000.0).floor() as i32,
            (position.y() / 1000.0).floor() as i32,
        )
    }

    pub fn nearby_blocks_including_self(&self) -> [BlockIndex; 9] {
        [
            self.top(),
            self.top_right(),
            self.clone(),
            self.right(),
            self.bottom_right(),
            self.bottom(),
            self.bottom_left(),
            self.left(),
            self.top_left(),
        ]
    }

    fn top(&self) -> Self {
        Self::new(self.x, self.y - 1)
    }

    fn top_right(&self) -> Self {
        Self::new(self.x + 1, self.y - 1)
    }

    fn right(&self) -> Self {
        Self::new(self.x + 1, self.y)
    }

    fn bottom_right(&self) -> Self {
        Self::new(self.x + 1, self.y + 1)
    }

    fn bottom(&self) -> Self {
        Self::new(self.x, self.y + 1)
    }

    fn bottom_left(&self) -> Self {
        Self::new(self.x - 1, self.y + 1)
    }

    fn left(&self) -> Self {
        Self::new(self.x - 1, self.y)
    }

    fn top_left(&self) -> Self {
        Self::new(self.x - 1, self.y - 1)
    }
}

fn redis_channel_for_block(block: &BlockIndex) -> String {
    format!("{}::{}:{}", WORLD_BLOCK_CHANNEL_PREFIX, block.x, block.y)
}

pub fn redis_connection_string() -> String {
    env::var("REDIS_CONNECTION_STRING")
        .unwrap_or_else(|_| "redis://redis.nikitavbv.com".to_owned())
}
