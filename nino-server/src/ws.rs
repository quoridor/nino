use std::sync::Arc;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};
use actix::prelude::*;
use actix::{Actor, Addr, ArbiterHandle, AsyncContext, Context, Running, StreamHandler};
use log::{info, error};
use nino_common::protocol;
use nino_common::protocol::{BubbleId, ClientUpdateMessage, WorldUpdateMessage};
use uuid::Uuid;
use rand::prelude::*;
use crate::redis::RedisWorldStateStore;
use crate::world::World;

const UPDATE_INTERVAL: Duration = Duration::from_millis(1000);
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

pub struct WsWorldSession {
    heartbeat: Instant,
    world: World,
}

impl Actor for WsWorldSession {
    type Context = actix_web_actors::ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.heartbeat(ctx);

        self.send_message(ctx, &WorldUpdateMessage::ClientInit {
            id: BubbleId::new(self.world.client_id().clone()),
        });

        self.send_message(ctx, &WorldUpdateMessage::BubbleCreate {
            id: BubbleId::new(self.world.client_id().clone()),
            position: protocol::Point::from(self.world.client_bubble().position()),
        });
    }

    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        Running::Stop
    }
}

impl StreamHandler<Result<actix_web_actors::ws::Message, actix_web_actors::ws::ProtocolError>> for WsWorldSession {
    fn handle(&mut self, msg: Result<actix_web_actors::ws::Message, actix_web_actors::ws::ProtocolError>, ctx: &mut Self::Context) {
        let msg = match msg {
            Err(_) => {
                ctx.stop();
                return;
            },
            Ok(msg) => msg,
        };

        match msg {
            actix_web_actors::ws::Message::Ping(msg) => {
                self.heartbeat = Instant::now();
                ctx.pong(&msg);
            },
            actix_web_actors::ws::Message::Pong(_) => {
                self.heartbeat = Instant::now();
            },
            actix_web_actors::ws::Message::Text(text) => {
                self.heartbeat = Instant::now();

                let message = text.to_string();
                let message: ClientUpdateMessage = serde_json::from_str(&message).unwrap();
                self.world.on_client_update(message);

                while let Some(update) = self.world.try_read_outbound_world_update() {
                    self.send_message(ctx, &update);
                }
            },
            actix_web_actors::ws::Message::Binary(_) => {
                error!("unexpected binary from websocket");
            },
            actix_web_actors::ws::Message::Close(reason) => {
                ctx.close(reason);
                ctx.stop();
            },
            actix_web_actors::ws::Message::Continuation(_) => {
                ctx.stop();
            },
            actix_web_actors::ws::Message::Nop => (),
        }
    }
}

impl WsWorldSession {

    pub fn new() -> Self {
        Self {
            heartbeat: Instant::now(),
            world: World::new(),
        }
    }

    fn heartbeat(&self, ctx: &mut actix_web_actors::ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.heartbeat) > CLIENT_TIMEOUT {
                info!("websocket client heartbeat timeout, disconnecting");

                ctx.stop();

                return;
            }

            ctx.ping(b"");
        });
    }

    fn send_message(&self, ctx: &mut <Self as Actor>::Context, message: &WorldUpdateMessage) {
        ctx.text(serde_json::to_string(message).unwrap());
    }
}
