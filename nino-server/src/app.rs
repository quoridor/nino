use std::time::Instant;
use actix_web::{App, HttpResponse, HttpServer, Responder, HttpRequest, web};
use actix_cors::Cors;
use actix_web::get;
use env_logger::Env;
use crate::ws::WsWorldSession;

pub async fn run() -> std::io::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    HttpServer::new(move || {
        App::new()
            .wrap(
                Cors::default()
                    .allow_any_origin()
                    .allow_any_header()
                    .allow_any_method(),
            )
            .service(health)
            .service(world_ws)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}

#[get("/api/v1/health")]
async fn health(_req: HttpRequest) -> impl Responder {
    HttpResponse::Ok().body("ok.")
}

#[get("/api/v1/world/ws")]
async fn world_ws(req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, actix_web::Error> {
    actix_web_actors::ws::start(
        WsWorldSession::new(),
        &req,
        stream
    )
}