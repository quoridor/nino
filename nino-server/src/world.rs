use std::time::Instant;
use crossbeam_channel::{Receiver, Sender, unbounded};
use mikey_geometry::{Point, Vector};
use nino_common::protocol;
use nino_common::protocol::{BubbleId, ClientUpdateMessage, FoodId, WorldUpdateMessage};
use rand::Rng;

const WORLD_MIN_X: f64 = 0.0;
const WORLD_MIN_Y: f64 = 0.0;
const WORLD_MAX_X: f64 = 2000.0;
const WORLD_MAX_Y: f64 = 2000.0;

const FOOD_CLUSTERS_PER_WORLD: u32 = 20;
const MIN_FOOD_CLUSTER_SIZE: u32 = 5;
const MAX_FOOD_CLUSTER_SIZE: u32 = 50;
const CLUSTER_SCATTER: f64 = 15.0;
const MIN_FOOD_MASS: f64 = 1.0;
const MAX_FOOD_MASS: f64 = 5.0;

pub struct World {
    this_bubble_id: String,
    bubbles: Vec<Bubble>,
    food: Vec<Food>,
    world_updates_tx: Sender<WorldUpdateMessage>,
    world_updates_rx: Receiver<WorldUpdateMessage>,
}

pub struct Bubble {
    id: String,
    updated_at: Instant,

    mass: f64,
    position: Point,
    movement_force: Vector,
}

pub struct Food {
    id: String,
    mass: f64,
    position: Point,
}

impl World {

    pub fn new() -> Self {
        let this_bubble_id = generate_bubble_id();
        let (world_updates_tx, world_updates_rx) = unbounded();

        let mut world = Self {
            this_bubble_id: this_bubble_id.clone(),
            bubbles: vec![
                Bubble::new(this_bubble_id, random_world_point())
            ],
            food: generate_initial_food_field(),
            world_updates_tx,
            world_updates_rx,
        };

        for food in &world.food {
            world.send_world_update(WorldUpdateMessage::FoodCreate {
                id: FoodId::new(food.id.clone()),
                position: protocol::Point::from(&food.position),
                mass: food.mass,
            })
        }

        world
    }

    pub fn on_client_update(&mut self, message: ClientUpdateMessage) {
        match message {
            ClientUpdateMessage::BubbleControlForceUpdate { force } => self.update_bubble_movement_force(&self.this_bubble_id.clone(), Vector::from(force)),
        }
    }

    fn update_bubble_movement_force(&mut self, bubble_id: &str, force: Vector) {
        if let Some(bubble) = self.bubbles.iter_mut().find(|bubble| bubble.id == bubble_id) {
            let current_time = Instant::now();
            let delta = (current_time - bubble.updated_at).as_millis() as f64;

            // TODO: think about sharing this formula between frontend and backend
            let new_position = &bubble.position + (&bubble.movement_force * delta * 64.0 / bubble.mass);
            bubble.position = new_position.clone();
            bubble.updated_at = Instant::now();
            bubble.movement_force = force;

            self.send_world_update(WorldUpdateMessage::BubblePositionUpdate {
                id: BubbleId::new(bubble_id.to_owned()),
                position: new_position.into(),
            });
        }
    }

    fn send_world_update(&self, message: WorldUpdateMessage) {
        self.world_updates_tx.send(message).unwrap();
    }

    pub fn add_bubble(&mut self, bubble: Bubble) {
        self.bubbles.push(bubble);
    }

    pub fn client_id(&self) -> &String {
        &self.this_bubble_id
    }

    pub fn client_bubble(&self) -> &Bubble {
        self.bubbles.iter().find(|bubble| &bubble.id == self.client_id()).unwrap()
    }

    pub fn try_read_outbound_world_update(&self) -> Option<WorldUpdateMessage> {
        self.world_updates_rx.try_recv().ok()
    }
}

impl Bubble {

    pub fn new(id: String, position: Point) -> Self {
        Self {
            id,
            updated_at: Instant::now(),
            mass: 100.0,
            position,
            movement_force: Vector::zero(),
        }
    }

    pub fn position(&self) -> &Point {
        &self.position
    }
}

impl Food {

    pub fn new(id: String, position: Point, mass: f64) -> Self {
        Self {
            id,
            position,
            mass,
        }
    }
}

fn generate_initial_food_field() -> Vec<Food> {
    let mut result = Vec::new();

    for _ in 0..FOOD_CLUSTERS_PER_WORLD {
        let cluster_center = random_world_point();
        let cluster_size = rand::thread_rng().gen_range(MIN_FOOD_CLUSTER_SIZE..MAX_FOOD_CLUSTER_SIZE);

        for _ in 0..cluster_size {
            let dx = rand::thread_rng().gen_range(-CLUSTER_SCATTER..CLUSTER_SCATTER);
            let dy = rand::thread_rng().gen_range(-CLUSTER_SCATTER..CLUSTER_SCATTER);
            let d_point = Point::new(dx, dy);

            let id = generate_food_id();
            let position = &cluster_center + d_point;
            let mass = rand::thread_rng().gen_range(MIN_FOOD_MASS..MAX_FOOD_MASS);

            result.push(Food::new(id, position, mass))
        }
    }

    result
}

fn generate_food_id() -> String {
    uuid::Uuid::new_v4().to_string()
}

fn generate_bubble_id() -> String {
    uuid::Uuid::new_v4().to_string()
}

fn random_world_point() -> Point {
    Point::new(
        rand::thread_rng().gen_range(WORLD_MIN_X..WORLD_MAX_X),
        rand::thread_rng().gen_range(WORLD_MIN_Y..WORLD_MAX_Y),
    )
}